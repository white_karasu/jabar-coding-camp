@extends('layout.master')
@section('user')
    Selamat datang admin
@endsection
@section('judul')
    Menu untuk menambah cast baru
@endsection
@section('content')
<a href="/cast" class="btn btn-primary my-3">Lihat Semua Cast</a> 
    <h2>Tambah Cast Baru</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast" required>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Cast" required>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label><br>
                <textarea name="bio" cols="30" required></textarea><br>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection