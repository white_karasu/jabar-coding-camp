@extends('layout.master')
@section('user')
    Selamat datang admin
@endsection
@section('judul')
    Daftar Cast
@endsection
@section('content')
<a href="/cast" class="btn btn-primary my-3">Lihat Cast Lain</a> 
    <h1>Nama: {{$cast->nama}}</h1>
    <p>Umur: {{$cast->umur}}</p>
    <p>Bio: {{$cast->bio}}</p>
@endsection