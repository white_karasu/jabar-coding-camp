@extends('layout.master')
@section('user')
    Selamat datang admin
@endsection
@section('judul')
    Daftar Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a> 
<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">  
                    </form>
                </td>
            </tr>
        @empty
            <h2>Data Kosong</h2>
        @endforelse
    </tbody>
  </table>
@endsection