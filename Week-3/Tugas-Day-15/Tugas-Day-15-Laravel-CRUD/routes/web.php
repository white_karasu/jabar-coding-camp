<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');


Route::get('/user_dashboard', function() 
    {
        return view('pages.user');
    });
Route::get('/table', function()
    {
        return view('pages.table');
    });
Route::get('/data-tables', function()
    {
        return view('pages.data-tables');
    });

//crud untuk tabel cast
//CREATE
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
//READ
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
//EDIT
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//UPDATE
Route::put('/cast/{cast_id}', 'CastController@update');
//DELETE
Route::delete('/cast/{cast_id}', 'CastController@destroy');