@extends('layout.master')
@section('judul')
    <h1>Buat Account Baru</h1>
@endsection
@section('content')
<h4>Sign Up Form</h4>
<form action="/welcome" method="post">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="first" required><br>
    <label>Last Name :</label><br>
    <input type="text" name="last" required><br>
    <label>Gender</label><br>
    <input type="radio" name="jk" required>
    <label>Male</label><br>
    <input type="radio" name="jk" required>
    <label>Female</label><br>
    <label>Nationality</label><br>
    <select name="Nationality" required>
        <option value="1">Indonesia</option>
        <option value="2">Jepang</option>
        <option value="3">Inggris</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="ls">
    <label>Bahasa Indonesia</label><br>
    <input type="checkbox" name="ls">
    <label>Inggris</label><br>
    <input type="checkbox" name="ls">
    <label>Other</label><br>
    <label>Bio</label><br>
    <textarea name="bio" cols="30" rows="10" required></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
    