<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.form');
    }
    public function welcome(Request $request)
    {
        //dd($request->all());
        $first = $request['first'];
        $last = $request['last'];

        return view('pages.hello', compact('first','last'));
    }
}
